<?php

use Helstern\Samknows\Infrastructure\Application\SilexApplication;
use Helstern\Samknows\Infrastructure\Application\Filesystem;

$appFileSystem = new Filesystem(__DIR__);
$app = new SilexApplication($appFileSystem);

return $app;
