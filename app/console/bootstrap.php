<?php

use Helstern\Samknows\Configuration\BootstrapperConsole;

$app = require __DIR__.'/../bootstrap.php';
$bootstrap = new BootstrapperConsole();
$bootstrap->bootstrap($app, empty($_ENV) ? $_SERVER : $_ENV, new \DateTime());

return $app;