#!/usr/bin/env php
<?php

use Helstern\Samknows\Infrastructure\Application\ConsoleApplication;

require_once __DIR__.'/../vendor/autoload.php';
set_time_limit(0);

$app = require __DIR__ . '/bootstrap.php';
/** @var ConsoleApplication $console */
$console = $app['console'];

$console->run();
