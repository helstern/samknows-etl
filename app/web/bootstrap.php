<?php
use Helstern\Samknows\Configuration\BootstrapperHttp;

$app = require __DIR__ . '/../bootstrap.php';
$bootstrap = new BootstrapperHttp();
$bootstrap->bootstrap($app, empty($_ENV) ? $_SERVER : $_ENV, new \DateTime());

return $app;