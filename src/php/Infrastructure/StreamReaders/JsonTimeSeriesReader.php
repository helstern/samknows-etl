<?php namespace Helstern\Samknows\Infrastructure\StreamReaders;

use Helstern\Samknows\Application;
use Helstern\Samknows\Domain;
use pcrov\JsonReader\JsonReader;

class JsonTimeSeriesReader implements Application\Import\DataReader
{
    /**
     * @param $source
     * @param Application\Import\Context $context
     * @return Domain\Metric\UnitTimeSeries[]
     * @throws \pcrov\JsonReader\Exception
     */
    public function readJson($source, Application\Import\Context $context)
    {
        $reader = new JsonReader();
        $reader->json($source);

        $timeSeries = $this->readStream($reader, $context);
        $reader->close();
        return $timeSeries;
    }

    /**
     * @param string $source
     * @param Application\Import\Context $context
     * @return Domain\Metric\UnitTimeSeries[]
     * @throws Application\Exception
     * @throws Application\LoggableException
     */
    public function read(string $source, Application\Import\Context $context): array
    {
        $reader = new JsonReader();
        $handle = null;
        try {
            $handle = fopen($source, 'r');
            $reader->stream($handle);

            $timeSeries = $this->readStream($reader, $context);
            $reader->close();
            return $timeSeries;
        } catch (\pcrov\JsonReader\InputStream\IOException $e) {
            throw Application\LoggableException::error('Failed to read from source 1', 0, $e);
        } catch (\pcrov\JsonReader\InvalidArgumentException $e) {
            throw Application\LoggableException::error('Failed to read from source 2', 0, $e);
        } catch (\pcrov\JsonReader\Exception $e) {
            throw new Application\Exception('Failed to read from source 3', 0, $e);
        } finally {
            if (is_resource($handle)) {
                fclose($handle);
            }
        }
    }

    /**
     * @param JsonReader $reader
     * @param Application\Import\Context $context
     * @return Domain\Metric\UnitTimeSeries[]
     * @throws \pcrov\JsonReader\Exception
     * @throws Application\Exception
     */
    public function readStream(JsonReader $reader, Application\Import\Context $context)
    {
        $timeSeries = [];

        $this->readStartArray($reader);
        $this->readStartObject($reader);
        do {
            $this->readUnit($reader, $context, $timeSeries);
        } while ($reader->type() === JsonReader::OBJECT);

        $this->readEndArray($reader);
        return $timeSeries;
    }

    /**
     * @param JsonReader $reader
     * @param Application\Import\Context $context
     * @param $timeSeries
     * @throws \pcrov\JsonReader\Exception
     * @throws Application\Exception
     */
    private function readUnit(JsonReader $reader, Application\Import\Context $context, &$timeSeries)
    {
        $reader->read('unit_id');
        if ($reader->type() !== JsonReader::NUMBER) {
            throw new \RuntimeException('invalid format: unexpected key unit_id ');
        }
        $unitId = $reader->value();

        $this->readStartObject($reader,'metrics');
        $this->readStartArray($reader);
        
        do {
            $metric = $reader->name();
            $builder = $context->createBuilder()->setUnitId($unitId)->setMetricType($metric);

            $this->readMeasurements($reader, $builder);
            $this->readEndArray($reader);

            $timeSeries[] = $builder->build();
        } while ($reader->type() === JsonReader::ARRAY);

        $this->readEndObject($reader);
        $this->readEndObject($reader);
    }

    /**
     * @param JsonReader $reader
     * @param Application\Import\UnitTimeSeriesBuilder $builder
     * @throws \pcrov\JsonReader\Exception
     */
    private function readMeasurements(JsonReader $reader, Application\Import\UnitTimeSeriesBuilder $builder)
    {
        $this->readStartObject($reader);
        do {

            $depth = $reader->depth();

            $value = null;
            $timestamp = null;

            while ($reader->read() && $reader->depth() > $depth) {
                if (
                    $reader->depth() === $depth + 1
                    && $reader->name() === 'value'
                    && $reader->type() === JsonReader::NUMBER
                ) {
                    $value = $reader->value();
                }

                if (
                    $reader->depth() === $depth + 1
                    && $reader->name() === 'timestamp'
                    && $reader->type() === JsonReader::STRING
                ) {
                    $timestamp = $reader->value();
                }
            }

            if (is_null($value) || is_null($timestamp)) {
                throw new \RuntimeException('Invalid format: missing required keys');
            }

            $builder->addMeasurement($timestamp, $value);
            $this->readEndObject($reader);

        } while ($reader->type() === JsonReader::OBJECT);
    }

    /**
     * @param JsonReader $reader
     * @param null $keyName
     * @throws \pcrov\JsonReader\Exception
     */
    private function readStartArray(JsonReader $reader, $keyName = null)
    {
        $reader->read($keyName);
        if ($reader->type() !== JsonReader::ARRAY ) {
            throw new \RuntimeException('invalid format: expecting start of array');
        }
    }

    /**
     * @param JsonReader $reader
     * @throws \pcrov\JsonReader\Exception
     */
    private function readEndArray(JsonReader $reader)
    {
        if ($reader->type() !== JsonReader::END_ARRAY) {
            throw new \RuntimeException('expecting end of array ');
        };

        $reader->read();
    }


    /**
     * @param JsonReader $reader
     * @param null $keyName
     * @throws \pcrov\JsonReader\Exception
     */
    private function readStartObject(JsonReader $reader, $keyName = null)
    {
        $reader->read($keyName);
        if ($reader->type() !== JsonReader::OBJECT ) {
            throw new \RuntimeException('invalid format: expecting key metrics to be an object');
        }
    }

    /**
     * @param JsonReader $reader
     * @throws \pcrov\JsonReader\Exception
     */
    private function readEndObject(JsonReader $reader)
    {
        if ($reader->type() !== JsonReader::END_OBJECT) {
            throw new \RuntimeException('expecting end of object ' . $reader->type());
        };

        $reader->read();
    }
}
