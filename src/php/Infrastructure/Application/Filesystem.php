<?php namespace Helstern\Samknows\Infrastructure\Application;

class Filesystem
{
    private $root;

    public function __construct($root)
    {
        $this->root = $root;
    }

    public function getCacheDir() : string
    {
        return $this->root . '/cache';
    }

    public function getLogsDir() : string
    {
        return $this->root . '/logs';
    }

    public function getTempDir() : string
    {
        return '/tmp';
    }

    public function getDbDir(string $relativePath = null) : string
    {
        $path = $this->root . '/db';
        if (is_null($relativePath)) {
            return $path;
        }

        return $path . '/' . $relativePath;
    }
}
