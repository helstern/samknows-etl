<?php namespace Helstern\Samknows\Infrastructure\Application;

class ErrorHandlerPriority
{
    const ERROR_REPORTER = -4;

    const AFTER_ERROR_REPORTER = -6;
}
