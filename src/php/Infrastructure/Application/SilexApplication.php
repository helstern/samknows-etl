<?php namespace Helstern\Samknows\Infrastructure\Application;

class SilexApplication extends \Silex\Application
{
    /** @var Filesystem */
    private $filesystem;

    /**
     * @param Filesystem $filesystem
     * @param array $values
     */
    public function __construct(Filesystem $filesystem, array $values = array())
    {
        parent::__construct($values);

        $this->filesystem = $filesystem;
    }

    /**
     * @return Filesystem
     */
    public function getFilesystem(): Filesystem
    {
        return $this->filesystem;
    }
}
