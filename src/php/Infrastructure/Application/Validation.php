<?php namespace Helstern\Samknows\Infrastructure\Application;

use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;

class Validation
{
    public static function createAnnotationClassMetadataFactory()
    {
        foreach (spl_autoload_functions() as $fn) {
            \Doctrine\Common\Annotations\AnnotationRegistry::registerLoader($fn);
        }
        $reader = new \Doctrine\Common\Annotations\AnnotationReader;
        $loader = new \Symfony\Component\Validator\Mapping\Loader\AnnotationLoader($reader);
        return new LazyLoadingMetadataFactory($loader);
    }
}
