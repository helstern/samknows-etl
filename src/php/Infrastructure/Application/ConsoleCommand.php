<?php namespace Helstern\Samknows\Infrastructure\Application;

use Helstern\Samknows\Infrastructure\Application;
use Symfony\Component\Console\Exception;
use Symfony\Component\Console\Command;
use Symfony\Component\Console\Input;

class ConsoleCommand extends Command\Command
{
    /** @var Application\SilexApplication */
    private $silexApplication;

    /**
     * @return Application\SilexApplication
     */
    public function getSilexApplication()
    {
        return $this->silexApplication;
    }

    /**
     * @param Application\SilexApplication $silexApplication
     */
    public function setSilexApplication(Application\SilexApplication $silexApplication)
    {
        $this->silexApplication = $silexApplication;
    }

    protected function validateOptions(Input\InputInterface $input)
    {
        $definition = $this->getDefinition();
        /** @var Input\InputOption[] $requiredOptions */
        $requiredOptions = [];
        foreach ($definition->getOptions() as $option) {
            if ($option->isValueRequired()) {
                $requiredOptions[] = $option;
            }
        }

        $missingOptions = [];

        $allOptions = $input->getOptions();
        foreach ($requiredOptions as $option) {
            $name = $option->getName();
            if (! array_key_exists($name, $allOptions) || empty($allOptions[$name])) {
                $missingOptions[] = $name;
            }
        }

        if (count($missingOptions) > 0) {
            throw new Exception\RuntimeException(sprintf('Not enough options (missing: "%s").', implode(', ', $missingOptions)));
        }
    }
}
