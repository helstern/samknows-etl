<?php namespace Helstern\Samknows\Infrastructure\Persistence;

use Carbon\Carbon;
use Doctrine\DBAL\Types\Type;
use Helstern\Samknows\Domain\Metric;
use Helstern\Samknows\Domain\TimeSeries;

class SchemaUtilsDoctrine
{
    /**
     * Creates the schema if it does not exist
     *
     * @param \Doctrine\DBAL\Schema\Schema $schema
     * @return SchemaUtilsDoctrine
     */
    public static function createSchema(\Doctrine\DBAL\Schema\Schema $schema)
    {
        foreach (Metric\Types::getTypeList() as $type) {
            SchemaUtilsDoctrine::createMetricFactsTable($schema, $type);
        }
        SchemaUtilsDoctrine::createHourDimensionTable($schema);
        SchemaUtilsDoctrine::createUnitDimensionTable($schema);
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    protected static function createUnitDimensionTable(\Doctrine\DBAL\Schema\Schema $schema)
    {
        $hourDimTable = $schema->createTable("unit_dim");
        $hourDimTable->addColumn("id", Type::STRING);
        $hourDimTable->addIndex(array("id"));
    }

    public static function tableNameUnitDimForTimeSeries(Metric\UnitTimeSeries $series) : string
    {
        return 'unit_dim';
    }

    /**
     * @param Metric\UnitTimeSeries $series
     * @return array
     */
    public static function timeSeriesToUnitDimRows(Metric\UnitTimeSeries $series): array
    {
        $mapper = function (TimeSeries\Item $item) use ($series) {
            return [
                'id' => $series->getUnitId()
            ];
        };

        return $series->map($mapper);
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    protected static function createHourDimensionTable(\Doctrine\DBAL\Schema\Schema $schema)
    {
        $hourDimTable = $schema->createTable("hour_dim");
        $hourDimTable->addColumn("id", Type::INTEGER, array("unsigned" => true));
        $hourDimTable->addColumn("hour", Type::INTEGER, array("unsigned" => true));
        $hourDimTable->setPrimaryKey(array("id"));
        $hourDimTable->addIndex(array("hour"));
    }


    /**
     * @param Metric\UnitTimeSeries $series
     * @return array
     */
    public static function timeSeriesToHourDimRows(Metric\UnitTimeSeries $series): array
    {
        $rows = [];
        $mapper = function (TimeSeries\Item $item) use ($rows) {
            $date = Carbon::createFromTimestamp($item->getTimestamp());
            return [
                'id' => Indexes::timeSeriesItemToIndex($item),
                'hour' => $date->hour
            ];
        };

        return $series->map($mapper);
    }

    public static function tableNameHourDimForTimeSeries(Metric\UnitTimeSeries $series) : string
    {
        return 'hour_dim';
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     * @param Metric\MetricType $metricType
     */
    protected static function createMetricFactsTable(\Doctrine\DBAL\Schema\Schema $schema, Metric\MetricType $metricType)
    {
        $metricTable = $schema->createTable($metricType->getName());

        $metricTable->addColumn('unit_dim_id', Type::STRING, array("length" => 32));
        $metricTable->addColumn('hour_dim_id', "integer", array("unsigned" => true));

        if ($metricType->isDecimalType()) {
            $metricTable->addColumn('value', Type::DECIMAL, array("precision" => 5, "scale" => 10));
        } else {
            $metricTable->addColumn('value', Type::INTEGER, array("unsigned" => true));
        }
    }

    /**
     * @param Metric\UnitTimeSeries $series
     * @return array
     */
    public static function timeSeriesToMetricFacts(Metric\UnitTimeSeries $series): array
    {
        $mapper = function (TimeSeries\Item $item) use ($series) {
            return [
                'unit_dim_id' => $series->getUnitId(),
                'hour_dim_id' => Indexes::timeSeriesItemToIndex($item),
                'value' => $item->getValue()
            ];
        };

        return $series->map($mapper);
    }

    /**
     * @param Metric\UnitTimeSeries $series
     * @return string
     */
    public static function tableNameMetricFactsForTimeSeries(Metric\UnitTimeSeries $series) : string
    {
        return SchemaUtilsDoctrine::tableNameMetricFactsForMetricType($series->getMetricType());
    }

    /**
     * @param Metric\MetricType $type
     * @return string
     */
    public static function tableNameMetricFactsForMetricType(Metric\MetricType  $type) : string
    {
        return $type->getName();
    }
}