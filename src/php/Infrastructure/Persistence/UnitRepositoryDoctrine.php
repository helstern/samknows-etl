<?php namespace Helstern\Samknows\Infrastructure\Persistence;

use Helstern\Samknows\Application\Services\UnitRepository;

class UnitRepositoryDoctrine implements UnitRepository
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $conn;

    /**
     * UnitTimeSeriesRepositorySqlLite constructor.
     *
     * @param \Doctrine\DBAL\Connection $connection
     */
    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->conn = $connection;
    }

    public function exists(string $id):bool
    {
        $statement = $this->conn->executeQuery('SELECT 1 FROM unit_dim WHERE id = ?', array($id));
        $exists = $statement->fetchColumn();

        return !!$exists;
    }
}