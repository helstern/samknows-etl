<?php namespace Helstern\Samknows\Infrastructure\Persistence;

use Helstern\Samknows\Domain\TimeSeries;
use Carbon\Carbon;

class Indexes
{
    /**
     * @param TimeSeries\Item $item
     * @return int
     */
    public static function timeSeriesItemToIndex(TimeSeries\Item $item) : int
    {
        $date = Carbon::createFromTimestamp($item->getTimestamp());
        return Carbon::parse($date->format('Y:m:d H:00:00'))->getTimestamp();
    }
}