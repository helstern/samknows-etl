<?php namespace Helstern\Samknows\Infrastructure\Persistence;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Helstern\Samknows\Domain\Metric;
use Helstern\Samknows\Domain\TimeSeries;

class MetricRepositoryDoctrine implements Metric\UnitTimeSeriesRepository
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $conn;

    /**
     * UnitTimeSeriesRepositorySqlLite constructor.
     *
     * @param \Doctrine\DBAL\Connection $connection
     */
    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->conn = $connection;
    }

    /**
     * @param Metric\UnitTimeSeries $series
     * @return mixed
     */
    public function add(Metric\UnitTimeSeries $series)
    {
        $tableName = SchemaUtilsDoctrine::tableNameUnitDimForTimeSeries($series);
        $rows = SchemaUtilsDoctrine::timeSeriesToUnitDimRows($series);
        // emulate upsert
        $this->upsert($tableName, $rows);


        $tableName = SchemaUtilsDoctrine::tableNameHourDimForTimeSeries($series);
        $rows = SchemaUtilsDoctrine::timeSeriesToHourDimRows($series);
        // emulate upsert
        $this->upsert($tableName, $rows);

        $tableName = SchemaUtilsDoctrine::tableNameMetricFactsForTimeSeries($series);
        $rows = SchemaUtilsDoctrine::timeSeriesToMetricFacts($series);

        // emulate batch insert
        $this->batchInsert($tableName, $rows);
    }

    /**
     * Emulates an upsert
     *
     * @param string $tableName
     * @param array $rows
     */
    protected function upsert(string $tableName, array $rows)
    {
        foreach ($rows as $row) {
            try {
                $this->conn->insert($tableName, $row);
            } catch (UniqueConstraintViolationException $e) {
                continue ;
            }
        }
    }

    /**
     * Emulates a batch insert
     *
     * @param string $tableName
     * @param array $rows
     */
    protected function batchInsert(string $tableName, array $rows)
    {
        foreach ($rows as $row) {
            $this->conn->insert($tableName, $row);
        }
    }

    /**
     * @param array|Metric\UnitTimeSeries[] $timeSeriesList
     * @return mixed
     */
    public function addAll(array $timeSeriesList)
    {
        foreach ($timeSeriesList as $timeSeries) {
            $this->add($timeSeries);
        }
    }

    /**
     * @param string $unitIdId
     * @param Metric\MetricType $metricType
     * @param string $hour
     * @param TimeSeries\TimeInterval $interval
     * @return TimeSeries\Item[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findHourlyTimeSeries(string $unitIdId, Metric\MetricType $metricType, string $hour, TimeSeries\TimeInterval $interval): array
    {
        $sql = sprintf('SELECT hour_dim_id as unix_timestamp, value FROM %s AS facts 
        INNER JOIN hour_dim ON facts.hour_dim_id = hour_dim.id 
        WHERE 
          hour_dim.hour = ? 
          AND hour_dim.id BETWEEN ? AND ?
          AND unit_dim_id = ?
        ', SchemaUtilsDoctrine::tableNameMetricFactsForMetricType($metricType)
        );
        $params = [
            intval($hour, 10),
            $interval->getStart(),
            $interval->getEnd(),
            $unitIdId
        ];

        $values = $this->fetchAll($sql, $params);
        $timeSeriesMapper = function (array $row) {
            return new TimeSeries\Item($row['value'], $row['unix_timestamp']);
        };
        return array_map($timeSeriesMapper, $values);
    }

    /**
     * @param string $sql
     * @param array $params
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function fetchAll(string $sql, array $params)
    {
        return $this->conn->executeQuery($sql, $params)->fetchAll();
    }
}
