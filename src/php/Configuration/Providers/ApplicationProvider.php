<?php namespace Helstern\Samknows\Configuration\Providers;

use Doctrine\DBAL\Connection;
use Helstern\Samknows\Application\Services\MetricService;
use Helstern\Samknows\Application\Services\UnitRepository;
use Helstern\Samknows\Domain\Metric\UnitTimeSeriesRepository;
use Helstern\Samknows\Infrastructure\Persistence\MetricRepositoryDoctrine;
use Helstern\Samknows\Infrastructure\Persistence\UnitRepositoryDoctrine;
use Helstern\Samknows\Infrastructure\StreamReaders\JsonTimeSeriesReader;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ApplicationProvider implements ServiceProviderInterface
{
    static public function createMetricService(Container $app):MetricService
    {
        /** @var Connection $connection */
        $timeSeriesRepository = $app[UnitTimeSeriesRepository::class];
        $unitRepository = $app[UnitRepository::class];
        $reader = new JsonTimeSeriesReader();
        return new MetricService($reader, $timeSeriesRepository, $unitRepository);
    }

    static public function createMetricRepository(Container $app) : MetricRepositoryDoctrine
    {
        /** @var Connection $connection */
        $connection = $app['db'];
        return new MetricRepositoryDoctrine($connection);
    }

    static public function createUnitRepository(Container $app) : UnitRepositoryDoctrine
    {
        /** @var Connection $connection */
        $connection = $app['db'];
        return new UnitRepositoryDoctrine($connection);
    }

    public function register(Container $app)
    {
        $app[MetricService::class] =  function (Container $app) {
            return ApplicationProvider::createMetricService($app);
        };

        $app[UnitTimeSeriesRepository::class] = function (Container $app) {
            return ApplicationProvider::createMetricRepository($app);
        };

        $app[UnitRepository::class] = function (Container $app) {
            return ApplicationProvider::createUnitRepository($app);
        };
    }
}
