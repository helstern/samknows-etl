<?php namespace Helstern\Samknows\Configuration\Providers;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Helstern\Samknows\Infrastructure\Application\ExceptionReporterPsr;
use Helstern\Samknows\Infrastructure\Serialization\JmsJsonSerializerBuilder;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class InfrastructureProvider implements ServiceProviderInterface, BootableProviderInterface
{
    public function register(Container $app)
    {
        // exception reporter
        $app->offsetSet('monolog.use_error_handler', true);
        $app['exception_reporter'] = function (Container $app) {
            /** @var LoggerInterface $logger */
            $logger = $app['logger'];
            return new ExceptionReporterPsr($logger);
        };

        // serialization
        if ($app->offsetExists("serializer.srcDir")) {
            $reflector = new ReflectionClass(\JMS\Serializer\Serializer::class);
            $srcDir = dirname($reflector->getFileName(), count((explode('\\', \JMS\Serializer\Serializer::class))));

            AnnotationRegistry::registerAutoloadNamespace("JMS\\Serializer\\Annotation", $srcDir);
        }
    }

    public function boot(Application $app)
    {
        // exception reporter
        $app['dispatcher']->addSubscriber($app['exception_reporter']);

        // serializer
        $app["serializer.json.builder"] = new JmsJsonSerializerBuilder();
        $app["serializer.json"] = function (Container $app) {
            return $app["serializer.json.builder"]->build();
        };
    }
}
