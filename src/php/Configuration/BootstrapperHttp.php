<?php namespace Helstern\Samknows\Configuration;

use Helstern\Samknows\HttpApi;
use Helstern\Samknows\Infrastructure\Validation;
use Helstern\Samknows\Infrastructure\Application;
use Helstern\Samknows\Manifest;
use Silex;

class BootstrapperHttp extends BootstrapperApplication
{
    public function bootstrap(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        parent::bootstrap($app, $env, $now);


        //cache
        $cacheDir = $app->getFilesystem()->getCacheDir();
        $app->register(new Silex\Provider\HttpCacheServiceProvider(), ['http_cache.cache_dir' => $cacheDir]);

        $appProvider = new HttpApi\ServiceProvider();
        $app->register($appProvider);
        $app->mount(Manifest::API_BASE_PATH, $appProvider);

        //allow service:method syntax when defining controllers
        $app->register(new Silex\Provider\ServiceControllerServiceProvider());
    }

}
