<?php namespace Helstern\Samknows\Configuration;

use Helstern\Samknows\Application\Services\MetricService;
use Helstern\Samknows\Application\ServicesProvider;
use Helstern\Samknows\Configuration\Providers;
use Helstern\Samknows\Infrastructure\Application;
use Helstern\Samknows\Infrastructure\StreamReaders\JsonTimeSeriesReader;
use Pimple\Container;
use Silex;

class BootstrapperApplication
{
    public function bootstrap(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $this->bootstrapEnv($app, $env, $now);

        //application services
        $this->bootstrapApplicationServices($app, $env, $now);

        //persistence services
        $this->bootstrapPersistenceServices($app, $env, $now);

        //infrastructure services
        $this->bootstrapInfrastructureServices($app, $env, $now);

    }

    private function bootstrapEnv(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $mapping = array_merge(
            ['debug' => Env::fqName(Env::DEBUG_MODE)],
            Env::getFQMapping()
        );
                           
        $this->mapEnvironmentValues($env, $mapping, $app);
    }

    private function mapEnvironmentValues(array $env, array $mapping, \ArrayAccess $mapInto)
    {
        $values = [];
        foreach ($mapping as $key => $envKey) {
            if (is_callable($envKey)) {
                $values[$key] = $envKey($env);
            } elseif (array_key_exists($envKey, $env)) {
                $values[$key] = $env[$envKey];
            }
        }
        
        foreach ($values as $key => $value) {
            $mapInto->offsetSet($key, $value);
        }
    }

    private function bootstrapApplicationServices(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $provider = new Providers\ApplicationProvider();
        $app->register($provider);
    }

    private function bootstrapPersistenceServices(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $dbDir = Env::valueOf(ENV::DB_ROOT, $env);
        $dbFileName = 'db.sqlite';
        $dbFilePath = empty($dbDir) ? $app->getFilesystem()->getDbDir($dbFileName) : realpath($dbDir) . DIRECTORY_SEPARATOR . $dbFileName;

        $app->register(new \Silex\Provider\DoctrineServiceProvider(), [
            'db.options' => [
                'driver' => 'pdo_sqlite',
                'path' => $dbFilePath
            ]
        ]);
    }

    private function bootstrapInfrastructureServices(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        // error handling, serialization
        $app->register(new Providers\InfrastructureProvider());

        //validation
        $app->register(
            new Silex\Provider\ValidatorServiceProvider(), ['validator.mapping.class_metadata_factory' => Application\Validation::createAnnotationClassMetadataFactory()]
        );

        //logging
        $logsDir = $app->getFilesystem()->getLogsDir();
        $monologConfiguration = [
            'monolog.use_error_handler' => false,
            'monolog.listener' => null,
            'monolog.logfile' => sprintf($logsDir.'/%s.log', $now->format('Y-m-d')),
            'monolog.level' => Env::valueOf(Env::LOG_SEVERITY, $env),
            'monolog.name' => 'application',
        ];
        $app->register(new Silex\Provider\MonologServiceProvider(), $monologConfiguration);
        $app->offsetUnset('monolog.listener');
    }
}
