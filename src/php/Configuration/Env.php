<?php namespace Helstern\Samknows\Configuration;

use Helstern\Samknows\Manifest;

class Env
{
    const DEBUG_MODE = 'DEBUG_MODE';

    const LOG_SEVERITY = 'LOG_SEVERITY';

    const DB_ROOT = 'DB_ROOT';

    static function valueOf(string $var, array $env): ?string
    {
        $fqName = Env::fqName($var);
        if (array_key_exists($fqName, $env)) {
            return (string) $env[$fqName];
        }

        return null;
    }

    /**
     * Returns the fully qualified name of an env variable
     *
     * @param string $name
     * @return string
     */
    static function fqName(string $name): string
    {
        return strtoupper(Manifest::IDENTIFIER) . '_' . strtoupper($name);
    }

    /**
     * Returns the mapping between variable name and fully qualified name
     *
     * @return array
     */
    static function getFQMapping(): array
    {
        return [
            Env::DEBUG_MODE => Env::fqName(Env::DEBUG_MODE),
            Env::LOG_SEVERITY => Env::fqName(Env::LOG_SEVERITY),
            Env::DB_ROOT => Env::fqName(Env::DB_ROOT),
        ];
    }

}
