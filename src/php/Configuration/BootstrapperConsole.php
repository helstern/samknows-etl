<?php namespace Helstern\Samknows\Configuration;

use Helstern\Samknows\ConsoleApi\DatabaseCommand;
use Helstern\Samknows\ConsoleApi\ImportCommand;
use Helstern\Samknows\Infrastructure\Application;
use Silex\Provider;

class BootstrapperConsole extends BootstrapperApplication
{
    public function bootstrap(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        parent::bootstrap($app, $env, $now);

        $app['console'] = function () use ($app) {
            $console = new Application\ConsoleApplication($app, 'Service Cli', '0.0.1');
            $console->setDispatcher($app['dispatcher']);
            $console->add(new DatabaseCommand('db'));
            $console->add(new ImportCommand('import'));

            return $console;
        };
    }
}
