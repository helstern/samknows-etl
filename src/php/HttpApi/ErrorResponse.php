<?php namespace Helstern\Samknows\HttpApi;

use JMS\Serializer\Annotation;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *   definition="ErrorResponse",
 *   type="object",
 *   required={"message"}
 * )
 * @Annotation\ExclusionPolicy("ALL")
 */
class ErrorResponse
{
    /**
     * @SWG\Property(type="string")
     * @Annotation\Type("string")
     * @Annotation\Expose()
     * @var string
     */
    private $message;

    /**
     * @SWG\Property(type="object")
     * @Annotation\Expose()
     * @var object;
     */
    private $details;

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return object|object[]
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param object|object[] $details
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }
}
