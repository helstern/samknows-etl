<?php namespace Helstern\Samknows\HttpApi;

use Pimple\ServiceProviderInterface;
use Silex\Api\ControllerProviderInterface;
use Silex\Application as SilexApplication;
use Silex\ControllerCollection;

interface ResourceProvider extends ServiceProviderInterface, ControllerProviderInterface
{
    public function connectWithCollection(SilexApplication $app, ControllerCollection $controllers);
}
