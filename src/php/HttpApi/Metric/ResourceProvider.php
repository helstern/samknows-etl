<?php namespace Helstern\Samknows\HttpApi\Metric;

use Pimple\Container;
use Silex\Application as SilexApplication;
use Silex\Controller;
use Silex\ControllerCollection;
use Helstern\Samknows\HttpApi;

class ResourceProvider implements HttpApi\ResourceProvider
{
    public function register(Container $app)
    {
        $app[StatsController::class] = function (Container $app) {
            return new StatsController();
        };

        $app[TimeSeriesController::class] = function (Container $app) {
            return new TimeSeriesController();
        };

        $app[Converter::class] = function (SilexApplication $app) {
            /** @var \JMS\Serializer\Serializer $serializer */
            $serializer = $app["serializer.json"];
            return new Converter($serializer);
        };
    }

    public function connectWithCollection(SilexApplication $app, ControllerCollection $controllers)
    {
        $statsHandler = implode(':', [StatsController::class, 'get']);
        $viewRenderer = implode(':', [Converter::class, 'statsToJson']);
        $app->view($viewRenderer);

        $controller = $controllers->get('/metrics/{metric}/stats', $statsHandler);
        $this->configureController($app, $controller);

        $timeSeriesHandler = implode(':', [TimeSeriesController::class, 'get']);
        $viewRenderer = implode(':', [Converter::class, 'timeSeriesToJson']);
        $app->view($viewRenderer);

        $controller = $controllers->get('/metrics/{metric}', $timeSeriesHandler);
        $this->configureController($app, $controller);

    }

    private function configureController(SilexApplication $app, Controller $controller) : ResourceProvider
    {
        $controller
            ->convert('metric', implode(':', [Converter::class, 'metricFromString']))
            ->convert('query', [Converter::class, 'queryFromRequest'])
        ;

        return $this;
    }

    /**
     * Returns routes to connect to the given application.
     *
     * @param SilexApplication $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(SilexApplication $app)
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory']; // creates a new controller based on the default route

        $this->connectWithCollection($app, $controllers);
        return $controllers;
    }
}
