<?php namespace Helstern\Samknows\HttpApi\Metric;

use Helstern\Samknows\Application\Services\MetricService;
use Helstern\Samknows\Domain\Metric\MetricType;
use Helstern\Samknows\Domain\TimeSeries\TimeInterval;
use Silex;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StatsController
{
    /**
     * @SWG\Get(
     *     path="/metric/{metric}/stats",
     *     operationId="stats-get",
     *     description="Queries the stats for a certain metric ",
     *     @SWG\Response(
     *         response=200,
     *         description="stats response")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="error"
     *     )
     * )
     * @param Request $request
     * @param Silex\Application $app
     * @param Query $query
     * @param MetricType $metric
     * @return \Helstern\Samknows\Application\Stats\SeriesStats
     */
    public function get(Request $request, Silex\Application $app, Query $query, MetricType $metric)
    {
        /** @var MetricService $service */
        $service = $app->offsetGet(MetricService::class);

        $stats = $service->retrieveStats(
            $query->getUnit(),
            $metric,
            $query->getHour(),
            new TimeInterval($query->getFrom(), $query->getTo())
        );

        if ($stats) {
            return $stats;
        }

        throw new NotFoundHttpException('no such dataset found');
    }
}
