<?php namespace Helstern\Samknows\HttpApi\Metric;

use Helstern\Samknows\Application\Services\MetricService;
use Helstern\Samknows\Domain\Metric\MetricType;
use Helstern\Samknows\Domain\TimeSeries\TimeInterval;
use Silex;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TimeSeriesController
{
    /**
     * @SWG\Get(
     *     path="/metric/{metric}",
     *     operationId="metric-get",
     *     description="Queries the time series values for a metric",
     *     @SWG\Response(
     *         response=200,
     *         description="list response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="error",
     *     )
     * )
     * @param Request $request
     * @param Silex\Application $app
     * @param Query $query
     * @param MetricType $metric
     * @return \Helstern\Samknows\Domain\Metric\UnitTimeSeries
     */
    public function get(Request $request, Silex\Application $app, Query $query, MetricType $metric)
    {
        /** @var MetricService $service */
        $service = $app->offsetGet(MetricService::class);

        $series = $service->retrieveTimeSeries(
            $query->getUnit(),
            $metric,
            $query->getHour(),
            new TimeInterval($query->getFrom(), $query->getTo())
        );

        if ($series) {
            return $series;
        }

        throw new NotFoundHttpException('no such dataset found');
    }
}
