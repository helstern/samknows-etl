<?php namespace Helstern\Samknows\HttpApi\Metric;

use Carbon\Carbon;
use Helstern\Samknows\Application\Stats\SeriesStats;
use Helstern\Samknows\Domain\Metric\MetricType;
use Helstern\Samknows\Domain\Metric\Types;
use Helstern\Samknows\Domain\Metric\UnitTimeSeries;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Converter
{
    /** @var Serializer */
    private $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function queryFromRequest($query, Request $request):Query
    {
        $query = new Query();

        $unit = $request->query->get('unit');
        if (empty($unit)) {
            throw new BadRequestHttpException('missing parameter: unit');
        }
        $query->setUnit((string) $unit);

        $to = $request->query->get('to');
        if (empty($to)) {
            throw new BadRequestHttpException('empty range parameter: from');
        }

        try {
           $to = Carbon::parse($to)->setTime(0, 0, 0)->timestamp;
           $query->setTo($to);
        } catch (\Exception $e) {
            throw new BadRequestHttpException('invalid date range parameter: from');
        }

        $from = $request->query->get('from');
        if (empty($from)) {
            throw new BadRequestHttpException('empty range parameter: from');
        }

        try {
            $from = Carbon::parse($from)->setTime(0, 0, 0)->timestamp;
            $query->setFrom($from);
        } catch (\Exception $e) {
            throw new BadRequestHttpException('invalid date range parameter: from');
        }

        $hour = $request->query->get('hour');
        if (is_null($hour)) {
            throw new BadRequestHttpException('missing parameter: hour');
        }

        $hour = intval($hour, 10);
        if ($hour >= 0 && $hour <= 24) {
            $hour = sprintf("%02d", $hour );
            $query->setHour($hour);
        } else {
            throw new BadRequestHttpException('invalid parameter: hour');
        }

        return $query;
    }

    public function metricFromString($metric, Request $request):MetricType
    {
        if (empty($metric)) {
            throw new BadRequestHttpException('missing required parameter: metric');
        }

        $metricType = Types::getMetricType($metric);
        if (empty($metricType)) {
            throw new NotFoundHttpException('metric not found: metric');
        }
        return $metricType;
    }

    public function statsToJson(SeriesStats $stats)
    {
        return $this->serializer->serialize($stats, 'json');
    }

    public function timeSeriesToJson(UnitTimeSeries $series)
    {
        return $this->serializer->serialize($series, 'json');
    }
}