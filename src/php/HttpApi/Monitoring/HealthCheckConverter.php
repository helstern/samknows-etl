<?php namespace Helstern\Samknows\HttpApi\Monitoring;

use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;

class HealthCheckConverter
{
    /** @var Serializer */
    private $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function convertToJson(HealthCheck $healthCheck, Request $request)
    {
        return $this->serializer->serialize($healthCheck, 'json');
    }
}
