<?php namespace Helstern\Samknows\HttpApi;

use Helstern\Samknows\Application;
use Helstern\Samknows\HttpApi\Monitoring;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\ControllerProviderInterface;
use Silex\Api\EventListenerProviderInterface;
use Silex\Application as SilexApplication;
use Silex\ControllerCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation;

class ServiceProvider implements ServiceProviderInterface, ControllerProviderInterface, EventListenerProviderInterface
{
    /** @var array ResourceProvider[] */
    private $resourceProviders = [];

    public function __construct()
    {
        $this->resourceProviders = [
            new Monitoring\ResourceProvider(),
            new Metric\ResourceProvider()
        ];
    }

    public function register(Container $app)
    {
        foreach ($this->resourceProviders as $provider) {
            $provider->register($app);
        }
    }

    /**
     * Returns routes to connect to the given application.
     *
     * @param SilexApplication $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(SilexApplication $app)
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory']; // creates a new controller based on the default route

        foreach ($this->resourceProviders as $provider) {
            $provider->connectWithCollection($app, $controllers);
        }

        $this->registerRequestContentTypeHandler($app);
        return $controllers;
    }

    public function subscribe(Container $app, EventDispatcherInterface $dispatcher)
    {
        $serializer = $app["serializer.json"];
        $subscriber = new ExceptionListener($serializer);
        $dispatcher->addSubscriber($subscriber);
    }

    private function registerRequestContentTypeHandler(SilexApplication $app)
    {
        $app->before(function (HttpFoundation\Request $request) {
            if (
                ! in_array($request->getMethod(), array('HEAD', 'GET'))
                && 0 !== strpos($request->headers->get('Content-Type'), 'application/json')

            ) {
                return new HttpFoundation\JsonResponse(
                    ['message' => 'unsupported media type'], HttpFoundation\Response::HTTP_UNSUPPORTED_MEDIA_TYPE
                );
            }

            return null;
        });
    }
}
