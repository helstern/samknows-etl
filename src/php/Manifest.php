<?php namespace Helstern\Samknows;

class Manifest
{
    const IDENTIFIER = 'SAMKNOWS';

    const API_BASE_PATH = '/api';

    const API_VERSION = '0.1.0';
}
