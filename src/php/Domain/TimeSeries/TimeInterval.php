<?php namespace Helstern\Samknows\Domain\TimeSeries;

class TimeInterval
{
    /**
     * @var int
     */
    private $start;

    /**
     * @var int
     */
    private $end;

    public function __construct (int $start, int $end)
    {
        $this->start = $start;
        
        $this->end = $end;
    }

    /**
     * @return int
     */
    public function getStart(): int
    {
        return $this->start;
    }

    /**
     * @return int
     */
    public function getEnd(): int
    {
        return $this->end;
    }

}