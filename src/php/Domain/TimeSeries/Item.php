<?php namespace Helstern\Samknows\Domain\TimeSeries;

class Item
{
    /** @var float  */
    private $value;

    /** @var int  */
    private $timestamp;

    public function __construct(float $value, int $timestamp)
    {
        $this->value = $value;
        $this->timestamp = $timestamp;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }
}