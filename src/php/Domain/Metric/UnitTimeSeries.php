<?php namespace Helstern\Samknows\Domain\Metric;

use Helstern\Samknows\Domain\TimeSeries;

class UnitTimeSeries
{
    /**
     * @var string
     */
    private $unitId;

    /**
     * @var MetricType
     */
    private $metricType;

    /**
     * @var array[]|TimeSeries\Item
     */
    private $timeSeries;

    /**
     * @var array[]|TimeSeries\Item
     */
    private $interval;

    /**
     * @var int
     */
    private $size;


    public function __construct(string $deviceId, MetricType $metricType, TimeSeries\TimeInterval $interval, array $timeSeries )
    {
        $this->unitId = $deviceId;
        $this->metricType = $metricType;
        $this->interval = $interval;
        $this->timeSeries = $timeSeries;
        $this->size = count($timeSeries);
    }

    /**
     * @return TimeSeries\TimeInterval
     */
    public function getInterval(): TimeSeries\TimeInterval
    {
        return $this->interval;
    }

    /**
     * @return string
     */
    public function getUnitId(): string
    {
        return $this->unitId;
    }

    public function getMetricType(): MetricType
    {
        return $this->metricType;
    }

    /**
     * @return int
     */
    public function size():int
    {
        return $this->size;
    }

    public function visit(Visitor $visitor)
    {
        foreach ($this->timeSeries as $item) {
            if ($this->metricType->isDecimalType()) {
                $visitor->visitDoubleValue($item->getValue(), $item->getTimestamp());
            } else {
                $visitor->visitIntegerValue($item->getValue(), $item->getTimestamp());
            }
        }
    }

    public function map(\Closure $mapper): array
    {
        return array_map($mapper, $this->timeSeries);
    }
}