<?php namespace Helstern\Samknows\Domain\Metric;

interface Visitor
{
    public function visitIntegerValue(int $value, int $timestamp);

    public function visitDoubleValue($value, int $timestamp);
}