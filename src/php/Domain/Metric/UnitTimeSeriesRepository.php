<?php namespace Helstern\Samknows\Domain\Metric;

use Helstern\Samknows\Domain\TimeSeries;

interface UnitTimeSeriesRepository
{
    /**
     * @param UnitTimeSeries $series
     * @return mixed
     */
    public function add(UnitTimeSeries $series);

    /**
     * @param array|UnitTimeSeries[] $timeSeriesList
     * @return mixed
     */
    public function addAll(array $timeSeriesList);

    /**
     * @param string $unitId
     * @param MetricType $metric
     * @param string $hour
     * @param TimeSeries\TimeInterval $interval
     * @return TimeSeries\Item[]|array
     */
    public function findHourlyTimeSeries(string $unitId, MetricType $metric, string $hour, TimeSeries\TimeInterval $interval) : array;
}
