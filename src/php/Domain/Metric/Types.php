<?php namespace Helstern\Samknows\Domain\Metric;


class Types
{
    const DOWNLOAD_SPEED = 'download';

    const UPLOAD_SPEED = 'upload';

    const LATENCY = 'latency';

    const PACKET_LOSS = 'packet_loss';

    /**
     * Map of metric types (flyweight).
     *
     * @var array|MetricType[]
     */
    private static $metricTypes = null;

    /**
     * @return array|MetricType[]
     */
    public static function getTypeList() {

        if (is_null(Types::$metricTypes)) {
            Types::$metricTypes = [
                new MetricType(Types::DOWNLOAD_SPEED),
                new MetricType(Types::UPLOAD_SPEED),
                new MetricType(Types::LATENCY),
                new MetricType(Types::PACKET_LOSS, true),
            ];
        }

        return Types::$metricTypes;
    }

    /**
     * @param string $name
     * @return MetricType|null
     */
    public static function getMetricType(string $name)
    {
        foreach (Types::getTypeList() as $type) {
            if ($type->getName() === $name) {
                return $type;
            }
        }
        return null;
    }

}