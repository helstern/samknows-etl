<?php namespace Helstern\Samknows\Domain\Metric;

class VisitorChain implements Visitor
{
    /**
     * @var array | Visitor[]
     */
    private $visitors;

    public function __construct(array $visitors)
    {
        $this->visitors = $visitors;
    }

    public function visitIntegerValue(int $value, int $timestamp)
    {
        foreach ($this->visitors as $visitor) {
            $visitor->visitIntegerValue($value, $timestamp);
        }
    }

    public function visitDoubleValue($value, int $timestamp)
    {
        foreach ($this->visitors as $visitor) {
            $visitor->visitDoubleValueValue($value, $timestamp);
        }
    }
}