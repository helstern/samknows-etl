<?php namespace Helstern\Samknows\Domain\Metric;

class MetricType
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $isDecimalType;

    /**
     * @param $name
     * @param $isDecimalType
     */
    public function __construct(string $name, bool $isDecimalType = false)
    {
        $this->name = $name;
        $this->isDecimalType = $isDecimalType;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isDecimalType(): bool
    {
        return $this->isDecimalType;
    }
}