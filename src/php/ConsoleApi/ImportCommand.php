<?php namespace Helstern\Samknows\ConsoleApi;

use Helstern\Samknows\Application\Services\MetricService;
use Helstern\Samknows\Infrastructure\Application\ConsoleCommand;
use Helstern\Samknows\Infrastructure\Persistence\SchemaUtilsDoctrine;
use Symfony\Component\Config\Tests\Util\Validator;
use Symfony\Component\Console\Input;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ImportCommand extends ConsoleCommand
{
    /**
     * TokenCommand constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDefinition([
                new InputArgument('source', InputArgument::REQUIRED, 'The url or file path of the data source')
            ])
            ->setDescription('Imports data from a source');
    }

    protected function resolveSource(InputInterface $input):string
    {
        $source = $input->getArgument('source' );
        /** @var Validator $validator */
        $validator = $this->getSilexApplication()->offsetGet('validator');
        $errors = $validator->validate($source, new Assert\Url());

        if (count($errors) === 0) {
            return $source;
        }

        $canonicPath = realpath($source);
        if (empty($canonicPath)) {
            throw new \InvalidArgumentException('could not resolve source');
        }
        return $canonicPath;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::validateOptions($input);
        $source = $this->resolveSource($input);

        try {
            /** @var MetricService $service */
            $service = $this->getSilexApplication()->offsetGet(MetricService::class);
            $service->import($source, ImportContext::testContext());
        } catch (\Exception $e) {
            echo($e->getMessage());
            return 1;
        }

        $output->writeln(sprintf('<info>Success: imported dataset from %s</info>', $source));
    }
}