<?php namespace Helstern\Samknows\ConsoleApi;

use Doctrine\DBAL;
use Doctrine\DBAL\Platforms;
use Doctrine\DBAL\Schema;
use Helstern\Samknows\Infrastructure\Application\ConsoleCommand;
use Helstern\Samknows\Infrastructure\Persistence\SchemaUtilsDoctrine;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DatabaseCommand extends ConsoleCommand
{
    /**
     * TokenCommand constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setDescription('Creates the service database. Currently only sqllite is the only supported platform ');
    }

    private function buildSql(Schema\Schema $schema, Platforms\AbstractPlatform $platform)
    {
        SchemaUtilsDoctrine::createSchema($schema);
        return $schema->toSql($platform);
    }

    /**
     * @return DBAL\Connection
     * @throws DBAL\DBALException
     */
    private function getConnection()
    {
        $config = new DBAL\Configuration();
        $options = $this->getSilexApplication()->offsetGet('db.options');
        return DBAL\DriverManager::getConnection($options, $config);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $conn = $this->getConnection();
        } catch (DBAL\DBALException $e) {
            $output->writeln('<error>Could not create a connection to the database</error>');
            return 1;
        }

        try {
            $statementList = $this->buildSql(new Schema\Schema(), new Platforms\SqlitePlatform());
            foreach ($statementList as $statement) {
                $conn->exec($statement);
            }
        } catch (\Exception $e) {
            $output->writeln('<error>Failed to create schema</error>');
            return 1;
        }

        $output->writeln('<info>Schema created</info>');
    }
}