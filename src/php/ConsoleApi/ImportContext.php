<?php namespace Helstern\Samknows\ConsoleApi;

use Carbon\Carbon;
use Helstern\Samknows\Application\Import;

class ImportContext implements Import\Context
{
    private $timeSeriesStart;

    private $timeSeriesStartEnd;

    public static function testContext():ImportContext
    {
        $start = Carbon::parse('2017-02-01')->timestamp;
        $end = Carbon::parse('2017-02-30')->timestamp;
        return new ImportContext($start, $end);
    }

    public function __construct(int $timeSeriesStart, int $timeSeriesEnd)
    {
        $this->timeSeriesStart = $timeSeriesStart;
        $this->timeSeriesStartEnd = $timeSeriesEnd;
    }

    function createBuilder():Import\UnitTimeSeriesBuilder
    {
        $builder = new Import\UnitTimeSeriesBuilder();
        $builder->setTimeInterval($this->timeSeriesStart, $this->timeSeriesStartEnd);
        return $builder;
    }
}