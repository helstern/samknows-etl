<?php namespace Helstern\Samknows\Application\Services;

use Helstern\Samknows\Application;
use Helstern\Samknows\Application\Stats;
use Helstern\Samknows\Domain\Metric;
use Helstern\Samknows\Domain\TimeSeries;

class MetricService
{
    /**
     * @var Application\Import\DataReader
     */
    private $reader;
    /**
     * @var Metric\UnitTimeSeriesRepository
     */
    private $repository;

    /**
     * @var UnitRepository
     */
    private $unitRepository;

    public function __construct(Application\Import\DataReader $reader, Metric\UnitTimeSeriesRepository $repository, UnitRepository $unitRepository)
    {
        $this->reader = $reader;
        $this->repository = $repository;
        $this->unitRepository = $unitRepository;
    }

    /**
     * @param string $source
     * @param Application\Import\Context $context
     * @throws Application\Exception
     */
    public function import(string $source, Application\Import\Context $context)
    {
        $dataSet = $this->reader->read($source, $context);
        $this->repository->addAll($dataSet);
    }

    /**
     * @param string $unitId
     * @param Metric\MetricType $metric
     * @param string $hour
     * @param TimeSeries\TimeInterval $interval
     * @return Metric\UnitTimeSeries|null
     */
    public function retrieveTimeSeries(string $unitId, Metric\MetricType $metric, string $hour, TimeSeries\TimeInterval $interval)
    {
        $unitHasTimeSeries = $this->unitRepository->exists($unitId);
        if ($unitHasTimeSeries) {
            $timeSeries = $this->repository->findHourlyTimeSeries($unitId,$metric, $hour, $interval);
            return new Metric\UnitTimeSeries($unitId, $metric, $interval, $timeSeries);
        }

        return null;
    }

    /**
     * @param string $unitId
     * @param Metric\MetricType $metric
     * @param string $hour
     * @param TimeSeries\TimeInterval $interval
     * @return Stats\SeriesStats|null
     */
    public function retrieveStats(string $unitId, Metric\MetricType $metric, string $hour, TimeSeries\TimeInterval $interval)
    {
        $unitHasTimeSeries = $this->unitRepository->exists($unitId);
        if (!$unitHasTimeSeries) {
            return null;
        }

        $timeSeries = $this->repository->findHourlyTimeSeries($unitId, $metric, $hour, $interval);
        $unitTimeSeries = new Metric\UnitTimeSeries($unitId, $metric, $interval, $timeSeries);
        return $this->calculateStats($unitTimeSeries);
    }

    /**
     * @param Metric\UnitTimeSeries $series
     * @return Stats\SeriesStats
     */
    public function calculateStats(Metric\UnitTimeSeries $series)
    {
        /** @var Metric\Visitor[]|Stats\Calculator[] $statsCalculators */
        $statsCalculators = [
            'min' => new Stats\VisitorCalculatorMinMax(true),
            'max' => new Stats\VisitorCalculatorMinMax(false),
            'mean' => new Stats\VisitorCalculatorMean(),
            'median' => new Stats\VisitorCalculatorMedian(),
        ];
        $series->visit(new Metric\VisitorChain($statsCalculators));

        $stats = new Stats\SeriesStats();
        foreach ($statsCalculators as $calculator) {
            $calculator->updateStats($stats);
        }

        return $stats;
    }
}
