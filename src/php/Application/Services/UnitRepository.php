<?php namespace Helstern\Samknows\Application\Services;

interface UnitRepository
{
    public function exists(string $id) : bool;
}