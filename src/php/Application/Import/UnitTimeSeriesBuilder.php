<?php namespace Helstern\Samknows\Application\Import;

use Carbon\Carbon;
use Helstern\Samknows\Application\Exception;
use Helstern\Samknows\Domain\TimeSeries;
use Helstern\Samknows\Domain\Metric;

class UnitTimeSeriesBuilder
{
    /**
     * @var Metric\MetricType
     */
    private $metricType;

    /**
     * @var string
     */
    private $unitId;

    /**
     * @var array|TimeSeries\Item[]
     */
    private $measurements;

    /**
     * @var TimeSeries\TimeInterval
     */
    private $timeInterval;

    /**
     * @param string $metricType
     * @return UnitTimeSeriesBuilder
     * @throws Exception
     */
    public function setMetricType(string $metricType): UnitTimeSeriesBuilder
    {
        $metricType = Metric\Types::getMetricType($metricType);
        if (is_null($metricType)) {
            throw new Exception(sprintf('unknown metric type: %s ', $metricType));
        }

        $this->metricType = $metricType;
        return $this;
    }

    /**
     * @param string|number $unitId
     * @return UnitTimeSeriesBuilder
     * @throws Exception
     */
    public function setUnitId($unitId): UnitTimeSeriesBuilder
    {
        $normalizedUnitId = (string) $unitId;
        if (empty($normalizedUnitId)) {
            throw new Exception(sprintf('unit it can not be empty'));
        }

        $this->unitId = $normalizedUnitId;
        return $this;
    }

    /**
     * @param string $timestamp
     * @param string|int|float $value
     * @return UnitTimeSeriesBuilder
     * @throws Exception
     */
    public function addMeasurement(string $timestamp, $value): UnitTimeSeriesBuilder
    {
        try {
            $timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, 'UTC')->timestamp;
        } catch (\InvalidArgumentException $e) {
            throw new Exception('can not parse timestamp', $e);
        }

        $value = floatval($value);
        $this->measurements[] = new TimeSeries\Item($value, $timestamp);

        return $this;
    }

    public function setTimeInterval(int $start, int $end)
    {
        $this->timeInterval = new TimeSeries\TimeInterval($start, $end);
        return $this;
    }

    public function build(): Metric\UnitTimeSeries
    {
        return new Metric\UnitTimeSeries($this->unitId, $this->metricType, $this->timeInterval, $this->measurements);
    }
}