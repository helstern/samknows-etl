<?php namespace Helstern\Samknows\Application\Import;

interface Context
{
    function createBuilder():UnitTimeSeriesBuilder;
}