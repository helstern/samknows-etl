<?php namespace Helstern\Samknows\Application\Import;

use Helstern\Samknows\Application\Exception;
use Helstern\Samknows\Domain\Metric\UnitTimeSeries;

interface DataReader
{
    /**
     * @param string $source
     * @param Context $context
     * @return UnitTimeSeries[]
     * @throws Exception
     */
    public function read(string $source, Context $context): array;
}
