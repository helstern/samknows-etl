<?php namespace Helstern\Samknows\Application\Stats;

use Helstern\Samknows\Domain\Metric;

/**
 * A median of medians implementation
 * @see https://www.ics.uci.edu/~eppstein/161/960130.html
 */
class VisitorCalculatorMedian implements Metric\Visitor, Calculator
{
    private $list = [];

    private $size = 0;

    private $median;

    private $partitionSize = 5;

    private $trivialSetSize = 10;

    public function visitIntegerValue(int $value, int $timestamp)
    {
        $this->visitNumeric($value, $timestamp);
    }

    public function visitDoubleValue($value, int $timestamp)
    {
        $this->visitNumeric($value, $timestamp);
    }

    private function visitNumeric($value, $timestamp)
    {
        $this->list[] = $value;
        $this->size++;
        $this->median = null;
    }

    /**
     * @return float|int
     * @throws \Exception
     */
    public function getCalculated()
    {
        if ($this->median) {
            return $this->median;
        }

        if ($this->size === 0) {
            throw new \Exception('illegal state: list is empty');
        }

        $rank = $this->determineRankOfMedian($this->size);
        $median = $this->find($this->list, intval($rank, 10));
        $this->median = $median;

        return $median;
    }

    /**
     * @param SeriesStats $seriesStats
     * @throws \Exception
     */
    public function updateStats(SeriesStats $seriesStats)
    {
        $value = $this->getCalculated();
        $seriesStats->setMedian($value);
    }

    /**
     * @param array $medians
     * @param array $set
     * @return array
     */
    private function reduceMedians(array $medians, array $set): array
    {
        sort($set);
        $size = count($set);
        $rank = $this->determineRankOfMedian($size);
        $medians[] = $set[$rank-1];

        return $medians;
    }

    private function determineRankOfMedian(int $size) : int
    {
        return $size % 2 ? ($size - 1) / 2 + 1 : $size / 2 + 1;
    }

    /**
     * @param array $list
     * @param int $rank
     * @return float|integer
     */
    private function find(array $list, int $rank)
    {
        $size = count($list);
        if ($size <= $this->trivialSetSize) {
            sort($list);
            return $list[$rank - 1];
        }

        $partitionReducer = function (array $medians, array $set) {
            return $this->reduceMedians($medians, $set);
        };
        $medians = array_reduce(array_chunk($list, $this->partitionSize), $partitionReducer, []);
        $median = $this->find($medians, $this->determineRankOfMedian(count($medians)));

        $less = [];
        $greater = [];
        $nrLess = 0;

        while(!is_null(key($list))) {
            $item = array_pop($list);
            if ($item < $median) {
                $less[] = $item;
                $nrLess++;
            } else if ($item > $median) {
                $greater[] = $item;
            }
        }

        if ($nrLess === $rank - 1) {
            return $median;
        }

        if ($nrLess > $rank -1) {
            return $this->find($less, $rank);
        }

        return $this->find($greater, $rank - 1 - $nrLess);
    }
}