<?php namespace Helstern\Samknows\Application\Stats;

use Helstern\Samknows\Domain\Metric;

class VisitorCalculatorMean implements Metric\Visitor, Calculator
{
    /** @var integer */
    private $total = 0;

    /** @var integer | double */
    private $sum = 0;

    public function visitIntegerValue(int $value, int $timestamp)
    {
        $this->visitNumeric($value, $timestamp);
    }

    public function visitDoubleValue($value, int $timestamp)
    {
        $this->visitNumeric($value, $timestamp);
    }

    private function visitNumeric($value, $timestamp)
    {
        $this->total++;
        $this->sum += $value;
    }

    /**
     * @return double
     */
    public function getCalculated()
    {
        if ($this->total > 0) {
            return $this->sum / $this->total;
        }

        return 0;
    }

    public function updateStats(SeriesStats $seriesStats)
    {
        $value = $this->getCalculated();
        $seriesStats->setMean($value);
    }
}