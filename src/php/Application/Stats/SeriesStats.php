<?php namespace Helstern\Samknows\Application\Stats;

class SeriesStats
{
    /**
     * @var integer | double
     */
    private $min;

    /**
     * @var integer | double
     */
    private $max;

    /**
     * @var integer | double
     */
    private $mean;

    /**
     * @var integer | double
     */
    private $median;

    /**
     * @return float|int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param float|int $min
     */
    public function setMin($min)
    {
        $this->min = $min;
    }

    /**
     * @return float|int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param float|int $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * @return float|int
     */
    public function getMean()
    {
        return $this->mean;
    }

    /**
     * @param float|int $mean
     */
    public function setMean($mean)
    {
        $this->mean = $mean;
    }

    /**
     * @return float|int
     */
    public function getMedian()
    {
        return $this->median;
    }

    /**
     * @param float|int $median
     */
    public function setMedian($median)
    {
        $this->median = $median;
    }
}