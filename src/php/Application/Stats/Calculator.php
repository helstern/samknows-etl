<?php namespace Helstern\Samknows\Application\Stats;

interface Calculator
{
    /**
     * @return float|int
     */
    public function getCalculated();

    /**
     * Updates the stats object with the calculated value
     *
     * @param SeriesStats $seriesStats
     * @return void
     */
    public function updateStats(SeriesStats $seriesStats);
}