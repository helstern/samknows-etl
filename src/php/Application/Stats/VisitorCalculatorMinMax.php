<?php namespace Helstern\Samknows\Application\Stats;

use Helstern\Samknows\Domain\Metric;

class VisitorCalculatorMinMax implements Metric\Visitor, Calculator
{
    /**
     * @var integer|double
     */
    private $current = null;

    /**
     * @var bool
     */
    private $calculateMin;

    public function __construct(bool $calculateMin = false)
    {
        $this->calculateMin = $calculateMin;
    }

    public function visitIntegerValue(int $value, int $timestamp)
    {
        $this->visitNumeric($value, $timestamp);
    }

    public function visitDoubleValue($value, int $timestamp)
    {
        $this->visitNumeric($value, $timestamp);
    }

    private function visitNumeric($value, $timestamp)
    {
        if (is_null($this->current)) {
            $this->current = $value;
            return;
        }

        if ($value < $this->current) {
            $this->current = $this->calculateMin ? $value : $this->current;
        } else if ($value > $this->current) {
            $this->current = $this->calculateMin ? $this->current : $value;
        }
    }

    /**
     * @return integer|double
     */
    public function getCalculated()
    {
        return $this->current;
    }

    public function updateStats(SeriesStats $seriesStats)
    {
        $calculated = $this->getCalculated();
        if ($this->calculateMin) {
            $seriesStats->setMin($calculated);
        } else {
            $seriesStats->setMax($calculated);
        }
    }
}