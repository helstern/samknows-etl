<?php namespace Helstern\Samknows\Application;

use Psr\Log\LogLevel;

class LoggableException extends Exception implements Loggable
{
    private $logLevel;

    /**
     * @param $message
     * @param $code
     * @param \Exception $previous
     * @return LoggableException
     */
    public static function emergency($message, $code, \Exception $previous)
    {
        return new LoggableException($message, $code, $previous, LogLevel::EMERGENCY);
    }
    /**
     * @param $message
     * @param $code
     * @param \Exception $previous
     * @return LoggableException
     */
    public static function alert($message, $code, \Exception $previous)
    {
        return new LoggableException($message, $code, $previous, LogLevel::ALERT);
    }
    /**
     * @param $message
     * @param $code
     * @param \Exception $previous
     * @return LoggableException
     */
    public static function critical($message, $code, \Exception $previous)
    {
        return new LoggableException($message, $code, $previous, LogLevel::CRITICAL);
    }
    /**
     * @param $message
     * @param $code
     * @param \Exception $previous
     * @return LoggableException
     */
    public static function error($message, $code, \Exception $previous)
    {
        return new LoggableException($message, $code, $previous, LogLevel::ERROR);
    }


    public function __construct($message, $code, \Exception $previous, string $logLevel)
    {
        parent::__construct($message, $code, $previous);
        $this->logLevel = $logLevel;
    }

    public function getSeverityLevel() : string
    {
        return $this->logLevel;
    }
}
