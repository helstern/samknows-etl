<?php namespace Helstern\Samknows\Application;

interface Loggable
{
    public function getSeverityLevel() : string;
}
