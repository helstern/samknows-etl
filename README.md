# ETL test

## Summary

  The project is organized in four layers, Presentation (Rest API and CLI), Domain, Application and Infrastructure, built on top Silex 
  The application layers uses a streaming json reader to parse the dataset into a list of domain objects which are the persisted with the help of an repository implemented in the 
  infrastructure layer
  
  The stats are calculated on demand and the median value calculation is using an implementation of the "median of medians" algorithm instead of the simpler sort to help deal with large(r) datasets as hinted in the description.
  The stats calculators can be switched or more can be added easily, as required       

## Environment

This project has been tested with php 7.1. To run it, the standard php 7.1 distribution should
be enough provided it has sqlite support. If not you will need to install the following packages:

    sudo apt-get install sqlite3 libsqlite3-dev
 

## Running the app

All the following commands assume the working dir is the root of the project.

0. Install the dependencies
    
    composer install

1. First, create the database:

    php app/console/app.php db
    
2. Run the import    

    php app/console/app.php import http://tech-test.sandbox.samknows.com/php-2.0/testdata.json  

3. Start a development server to begin querying

    php -S localhost:8000 app/web/app.php
    
4. Run queries:

    curl http://localhost:8000/api/metric/download?unit=1&hour=2&from=2017-02-01&to=2017-02-30 -H "Accept: application/json"
    
    curl http://localhost:8000/api/metric/download/stats?unit=1&hour=2&from=2017-02-01&to=2017-02-30 -H "Accept: application/json"       

## Testing

The api and unit tests can be run with the following command:

    php app/vendor/bin/phpunit
            
## Todo

 - better handling of the import: use staging tables, dropping and rebuilding indexes. Move all queries in the database and use stored procedures to exchange data, reason being it would give the data team much more control over optimizations
 - in the api layer, map the domain and application objects to DTOs for serialization
 - more api tests                     
 - store the stats in the database after they were calculated instead of computing them on the fly. It is not clear if the import keeps adding new rows, and in this case i would move the calculations into the database.
 - have one endpoint to return the stats and the time series for a data set
 - implement the monitoring endpoint 