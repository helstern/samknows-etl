<?php namespace Helstern\Samknows;

use Doctrine\DBAL\Connection;
use Helstern\Samknows\Configuration\Env;
use Silex\WebTestCase;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class TestCase extends WebTestCase
{
    public function getResourceDir()
    {
        return realpath(__DIR__.'/../../resources');
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function setUp()
    {
        parent::setUp();
        /** @var Connection $conn */
        $conn = $this->app['db'];
        $file = file(realpath(__DIR__.'/../../../src/schemas/test.sqllite.sql'));
        foreach ($file as $statement) {
            $conn->exec($statement);
        }

    }

    protected function tearDown()
    {
        /** @var Connection $conn */
        $dbPath = $this->app['db.options']['path'];
        unlink($dbPath);
    }

    /**
     * Creates the application.
     *
     * @return HttpKernelInterface
     */
    public function createApplication()
    {
        if (empty($_ENV)) {
            $_SERVER[ENV::fqName(ENV::DB_ROOT)] = $this->getResourceDir();
        } else {
            $_ENV[ENV::fqName(ENV::DB_ROOT)] = $this->getResourceDir();
        }

        $config = __DIR__.'/../../../app/web/bootstrap.php';
        $configPath = realpath($config);
        $app = require $configPath;
        $app['debug'] = true;
        unset($app['exception_handler']);
        return $app;
    }
}