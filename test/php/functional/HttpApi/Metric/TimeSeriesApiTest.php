<?php namespace Helstern\Samknows\HttpApi\Metric;

use Helstern\Samknows\TestCase;
use Symfony\Component\BrowserKit\Client;

class TimeSeriesApiTest extends TestCase
{
    public function testReturns400WhenMissingRequiredParameter()
    {
        $client = $this->createClient();
        $crawler = $client->request(
            'GET'
            , '/api/metrics/download?unit=3&hour=2&from=2017-02-01'
            , []
            , []
            , ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testReturns404WhenUnitNotExisting()
    {
        $client = $this->createClient();
        $crawler = $client->request(
            'GET'
            , '/api/metrics/download?unit=300&hour=2&from=2017-02-01&to=2017-02-30'
            , []
            , []
            , ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testReturnsExpectedRepresentationWhenUnitDoesNotHaveTimeSeries()
    {
        $client = $this->createClient();
        $crawler = $client->request(
            'GET'
            , '/api/metrics/download?unit=1&hour=2&from=2017-02-01&to=2017-02-30'
            , []
            , []
            , ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $actualContent = $client->getResponse()->getContent();
        $actualContent = json_encode(json_decode($actualContent));

        $expectedContent = '{"unit_id":"1","metric_type":{"name":"download","is_decimal_type":false},"time_series":[],"interval":{"start":1485907200,"end":1488412800},"size":0}';
        $this->assertEquals($expectedContent, $actualContent);
    }


}
