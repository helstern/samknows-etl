<?php namespace Helstern\Samknows\HttpApi\Metric;

use Helstern\Samknows\TestCase;
use Symfony\Component\BrowserKit\Client;

class StatsApiTest extends TestCase
{
    public function testReturns400WhenMissingRequiredParameter()
    {
        $client = $this->createClient();
        $crawler = $client->request(
            'GET'
            , '/api/metrics/download/stats?&hour=2&from=2017-02-01&to=2017-02-30'
            , []
            , []
            , ['CONTENT_TYPE' => 'application/json']
        );
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }
}
