<?php namespace Helstern\Samknows\Infrastructure\StreamReaders;

use Helstern\Samknows\ConsoleApi\ImportContext;
use Helstern\Samknows\Infrastructure\DataSet\JsonStreamTimeSeriesReader;
use pcrov\JsonReader\JsonReader;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonTimeSeriesReaderTest extends \PHPUnit_Framework_TestCase
{
    public function testLeadNotAddedWhenPreviousFound()
    {
        $json = <<<'JSON'
[
  {
    "unit_id": 1,
    "metrics": {
      "download": [
        {
          "timestamp": "2017-02-16 05:00:00",
          "value": 4670240
        }
      ],
      "upload": [
        {
          "timestamp": "2017-02-04 11:00:00",
          "value": 1185600
        },
        {
          "timestamp": "2017-02-04 17:00:00",
          "value": 1214640
        }
      ]
    }
  },
  {
    "unit_id": 2,
    "metrics": {
      "packet_loss": [
        {
          "timestamp": "2017-02-26 16:00:00",
          "value": 0.26
        }
      ]
    }
  }
]
JSON;

        $reader = new JsonTimeSeriesReader();
        $readerContext = ImportContext::testContext();
        $unitMetrics = $reader->readJson($json, $readerContext);

        $this->assertEquals(3, count($unitMetrics));
    }
}
