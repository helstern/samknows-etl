<?php namespace Helstern\Samknows\Application\Import;

use Helstern\Samknows\Application\Exception;
use Helstern\Samknows\Domain\Metric\Types;

class UnitTimeSeriesBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testSetMetricTypeThrowsExceptionWhenMetricTypeIsUnknonw()
    {
        $metric = strrev(Types::PACKET_LOSS);
        $this->assertNull(Types::getMetricType($metric));

        $builder = new UnitTimeSeriesBuilder();
        try {
            $builder->setMetricType($metric);
        } catch (Exception $e) {
            $expectedException = $e;
        }
        $this->assertNotNull($expectedException);
    }
}