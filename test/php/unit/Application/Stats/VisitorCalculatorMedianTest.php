<?php namespace Helstern\Samknows\Application\Stats;

class VisitorCalculatorMedianTest extends \PHPUnit_Framework_TestCase
{
    public function testFindsMedianForNonTrivialCases()
    {
        $smallSet = [1, 2, 3, 4, 5, 6 ,7 ,8, 9, 10, 11];
        shuffle($smallSet);

        $sets = [
            $smallSet,
            [1, 2, 3, 4, 5, 6 ,7 ,8, 9, 10, 11, 12]
        ];

        $medians = [];
        foreach ($sets as $set) {
            $sorted = array_merge([], $set);
            sort($sorted);
            $size = count($sorted);
            $medians[] = $size % 2 ? $sorted[($size - 1) / 2] : $sorted[$size / 2];
        }

        for ($i = 0 ; $i < count($sets); $i++) {
            $set = $sets[0];
            $expectedMedian = $medians[0];

            $calculator = new VisitorCalculatorMedian();
            foreach ($set as $value) {
                $calculator->visitIntegerValue($value, 1);
            }

            $median = $calculator->getCalculated();
            $this->assertEquals($expectedMedian, $median);
        }
    }
}